from django.db import models


class Users(models.Model):
    idusers = models.AutoField(db_column='idUsers', primary_key=True)  # Field name made lowercase.
    username = models.CharField(unique=True, max_length=45)
    name = models.CharField(max_length=45)
    privileges = models.PositiveIntegerField()
    municipality = models.CharField(max_length=45)
    email = models.CharField(max_length=45)

    class Meta:
        db_table = 'users'


class Logs(models.Model):
    # Field name made lowercase.
    idlogs = models.AutoField(db_column='idLogs', primary_key=True)
    action = models.CharField(max_length=45)
    details = models.CharField(max_length=45)
    log_timestamp = models.DateTimeField()
    # Field name made lowercase.
    iduser = models.ForeignKey('users', models.DO_NOTHING, db_column='idUser')

    class Meta:
        db_table = 'logs'
