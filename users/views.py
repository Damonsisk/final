from django.shortcuts import render, render_to_response, redirect
from .models import Logs, Users
from data_manage.models import WasteType
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate, login, update_session_auth_hash, logout
from django.contrib.auth.models import User
from django.template import RequestContext
from django.contrib import messages
import requests

def profile(request):
    username = request.user.username
    curr_user = Users.objects.get(username=username)

    context = {'curr_user': curr_user}
    return render(request, 'users/profile.html', context)

@csrf_exempt
def auth_user(request):
    if request.is_ajax and request.method == 'POST':
        usr = request.POST['username']
        pwd = request.POST['password']

        creds = {'username': usr, 'password': 'pwd'}

        r = requests.post('http://127.0.0.1:8000/api/token/', data=creds)
      
        if r.ok:
            print('ok')
            print(r.text)
        
        return render(request, 'users/profile.html', {})


def change_password(request):
    wt = WasteType.objects.all()
    data = {'wt': wt}

    if request.method == 'POST':
        user = authenticate(username=request.user, password=request.POST['old_password'])
        if user is not None:
            if request.POST['new_password1'] == request.POST['new_password2']:
                u = User.objects.get(username=request.user)
                u.set_password(request.POST['new_password1'])
                u.save()

                update_session_auth_hash(request, user)

                return render(request, 'users/settings.html', {})
            else:
                print('passes dont match')
        else:
            print('wrong pass')
    else:
        return render(request, 'users/settings.html', data)

def settings(request):
    wt = WasteType.objects.all()
    data = {'wt': wt}

    return render(request, 'users/settings.html', data)


def logs(request):
    username = request.user.username
    userid = Users.objects.get(username=username).pk

    inst = Logs.objects.filter(iduser=userid)
    data = {'logs': inst}
    return render(request, 'users/logs.html', data)

def forgot_password(request):
    return render(request, 'users/forgot_password.html', {})