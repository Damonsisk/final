from django.contrib import admin
from .models import Users, Logs


@admin.register(Users)
class UsersAdmin(admin.ModelAdmin):
    list_display = ("idusers", "username",
                    "name", "privileges")


@admin.register(Logs)
class LogsAdmin(admin.ModelAdmin):
    list_display = ("idlogs", "action",
                    "details", "log_timestamp", "iduser")
