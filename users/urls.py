from django.contrib import admin
from django.urls import path, include, re_path
from django.conf.urls import url
from . import views

urlpatterns = [
    re_path(r'profile/?', views.profile, name='user_profile'),
    re_path(r'settings/?', views.settings, name='user_settings'),
    re_path(r'logs/?', views.logs, name='user_logs'),
    path('change_password/', views.change_password, name='change_password'),
    # url(r'^password/$', views.change_password, name='change_password'),
    path('test_token/', views.auth_user, name='auth_user'),
    path('forgot_password/', views.forgot_password, name='forgot_password'),
]
