-- MySQL dump 10.13  Distrib 8.0.21, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: WasteDashboard
-- ------------------------------------------------------
-- Server version	8.0.21-0ubuntu0.20.04.4

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Logs`
--

DROP TABLE IF EXISTS `Logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Logs` (
  `idLogs` int unsigned NOT NULL AUTO_INCREMENT,
  `action` varchar(45) NOT NULL,
  `details` varchar(45) NOT NULL,
  `log_timestamp` datetime NOT NULL,
  `idUser` int unsigned NOT NULL,
  PRIMARY KEY (`idLogs`),
  UNIQUE KEY `idLogs_UNIQUE` (`idLogs`),
  KEY `fk_logs_user_idx` (`idUser`),
  CONSTRAINT `fk_logs_user` FOREIGN KEY (`idUser`) REFERENCES `Users` (`idUsers`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Municipalities`
--

DROP TABLE IF EXISTS `Municipalities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Municipalities` (
  `idMunicipalities` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`idMunicipalities`),
  UNIQUE KEY `idMunicipalities_UNIQUE` (`idMunicipalities`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Users` (
  `idUsers` int unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `privileges` int unsigned NOT NULL,
  `idMunicipality` int unsigned NOT NULL,
  `password` varchar(455) NOT NULL,
  PRIMARY KEY (`idUsers`),
  UNIQUE KEY `idUsers_UNIQUE` (`idUsers`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  KEY `fk_users_municipality_idx` (`idMunicipality`),
  CONSTRAINT `fk_users_municipality` FOREIGN KEY (`idMunicipality`) REFERENCES `Municipalities` (`idMunicipalities`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Waste_buckets`
--

DROP TABLE IF EXISTS `Waste_buckets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Waste_buckets` (
  `idWaste_buckets` int unsigned NOT NULL AUTO_INCREMENT,
  `waste_type` varchar(45) NOT NULL,
  `idMunicipality` int unsigned NOT NULL,
  `buckets_num` int unsigned NOT NULL,
  `last_update` datetime NOT NULL,
  PRIMARY KEY (`idWaste_buckets`),
  UNIQUE KEY `idWaste_buckets_UNIQUE` (`idWaste_buckets`),
  KEY `fk_waste_buckets_municipality_idx` (`idMunicipality`),
  CONSTRAINT `fk_waste_buckets_municipality` FOREIGN KEY (`idMunicipality`) REFERENCES `Municipalities` (`idMunicipalities`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Waste_data`
--

DROP TABLE IF EXISTS `Waste_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Waste_data` (
  `idWaste_data` int unsigned NOT NULL AUTO_INCREMENT,
  `waste_type` varchar(45) NOT NULL,
  `idMunicipality` int unsigned NOT NULL,
  `waste_kg` decimal(10,2) NOT NULL,
  `data_added` date NOT NULL,
  PRIMARY KEY (`idWaste_data`),
  UNIQUE KEY `idWaste_data_UNIQUE` (`idWaste_data`),
  KEY `fk_waste_data_municipality_idx` (`idMunicipality`),
  CONSTRAINT `fk_waste_data_municipality` FOREIGN KEY (`idMunicipality`) REFERENCES `Municipalities` (`idMunicipalities`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-12 12:49:35
