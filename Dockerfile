FROM python:3.6

ENV PYTHONUNBUFFERED 1

RUN mkdir /main

WORKDIR /main

COPY requirements.txt /main

RUN pip install -r /main/requirements.txt

COPY . /main/
