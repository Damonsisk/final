from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from data_manage.models import WasteType

#import pandas as pd
#import numpy as np
#import matplotlib.pyplot as plt
#import scipy as sp
#from sklearn import model_selection, preprocessing, feature_selection, ensemble, linear_model, metrics, decomposition


def predictions_home(request):
    wt = WasteType.objects.all().order_by('waste_greek')
    
    return render(request, 'predictions/predictions.html', {'wt': wt})

def ml_predictions(request):
    #load data
    #split train, test sets
    #feature selection
    #linear regression model
    #train
    #predict
    #evaluate
    return None