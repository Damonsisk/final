from django.db import models

class PredictionsMonthly(models.Model):
    idpredictions_month = models.AutoField(db_column='idPredictions_month', primary_key=True)
    waste_type = models.CharField(max_length=45)
    tsda_pred = models.DecimalField(max_digits=10, decimal_places=2)
    ml_pred = models.DecimalField(max_digits=10, decimal_places=2)
    month = models.PositiveIntegerField()
    year = models.PositiveIntegerField()

    class Meta:
        db_table = 'predictions_monthly'


class PredictionsYear(models.Model):
    idpredictions_year = models.AutoField(db_column='idPredictions_year', primary_key=True)
    waste_type = models.CharField(max_length=45)
    tsda_pred = models.DecimalField(max_digits=10, decimal_places=2)
    ml_pred = models.DecimalField(max_digits=10, decimal_places=2)
    year = models.PositiveIntegerField()

    class Meta:
        db_table = 'predictions_year'