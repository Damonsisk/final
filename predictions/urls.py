from django.contrib import admin
from django.urls import path, include, re_path
from . import views

urlpatterns = [
    re_path(r'predictions/?', views.predictions_home, name='predictions_home'),
]
