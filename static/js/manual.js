function initFooter()
{      
  var lnkFeedback = document.getElementById('lnkFeedback');
  if (lnkFeedback)
  { // Add topic URL to the "Send feedback" link
    var href = "";
    try
    {
      var topLevelParent = window.top;
      lnkFeedback.href += '?Subject=Feedback on ' 
        + encodeURIComponent(topLevelParent.window.location);
    }
    catch(e)
    {
    }
  }
}