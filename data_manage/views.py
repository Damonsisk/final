from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from data_manage.models import WasteData, WasteBuckets, WasteType, DataPeriod, FileManager
from datetime import datetime
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from django.core.files.storage import default_storage
from django.contrib.staticfiles.utils import get_files
from django.http import FileResponse
import xlrd
import pandas as pd
import uuid
import requests
import os
import boto3

months = ['Ιανουάριος', 'Φεβρουάριος', 'Μάρτιος', 'Απρίλιος', 'Μάιος', 'Ιούνιος',
          'Ιούλιος', 'Αύγουστος', 'Σεπτέμβριος', 'Οκτώβριος', 'Νοέμβριος', 'Δεκέμβριος']

days = ['Δευτέρα', 'Τρίτη', 'Τετάρτη', 'Πέμπτη', 'Παρασκευή', 'Σάββατο', 'Κυριακή']


def year_data(request, year):
    wt = WasteType.objects.all().order_by('idwaste_translation').values()

    la = ra = 1
    yb = year-1
    ya = year+1

    min_year = 2015
    max_year = datetime.now().year

    type_data = []
    for i in wt:
        data = { 'type' : i['waste_greek'], 'year_total' : 0, 'month_total' : [] }
        type_data.append(data)

    for t in type_data:
        t['month_total'] = [0] * 12
        for m in range(1, 12+1):
            d_dics = WasteData.objects.filter(waste_type=t['type'], date_added__year=year, date_added__month=m).values('waste_kg')
            d_old = [d['waste_kg'] for d in d_dics]
            d = [float(j) for j in d_old]
            
            if d:
                t['month_total'][m-1] += round(sum(d), 2)
        
        t['year_total'] = round(sum(t['month_total']), 2) / 1000 # convert kg to tn
    
    month_total = [0] * 12
    for i in type_data:
        for m in range(0, 12):
            month_total[m] += i['month_total'][m]

    if min_year <= year <= max_year:
        if year == min_year:
            la = 0
        elif year == max_year:
            ra = 0
    else:
        return render(request, 'main/error_404.html', {})

    year_total = 0
    for t in type_data:
        year_total += t['year_total']
    year_total = round(year_total, 2)

    type_perc_pie = {'type' : [], 'perc': []}
    for t in type_data:
        type_perc_pie['type'].append(t['type'])
        if (year_total):
            type_perc_pie['perc'].append(round((t['year_total'] * 100 / year_total), 2))
        else:
            type_perc_pie['perc'].append(0)

    context = {'months': months, 'wt': wt, 'type_data' : type_data, 'month_total' : month_total,
               'year': year, 'la': la, 'ra': ra, 'yb': yb, 'ya': ya, 'type_perc_pie': type_perc_pie, 'year_total' : year_total}

    return render(request, 'data_manage/year_graph.html', context)            


def waste_info(request, wtype):
    wti = WasteType.objects.get(idwaste_translation=wtype)
    wt = WasteType.objects.all()

    year = datetime.now().year
    min_year = 2014
    max_year = year
    years = [*range(min_year, max_year)]

    data = []
    year_totals = []
    for y in years:
        temp = {'month_total': [], 'year_total' : 0, 'year': y}
        temp['month_total'] = [0] * 12
        
        for m in range(1, 12+1):
            d_dics = WasteData.objects.filter(waste_type=wti.waste_greek, date_added__year=y, date_added__month=m).values('waste_kg')
            d_old = [d['waste_kg'] for d in d_dics]
            d = [float(j) for j in d_old]
            
            if d:
                temp['month_total'][m-1] += round(sum(d), 2)
            
        temp['year_total'] = round(sum(temp['month_total']), 2) /1000
        year_totals.append(temp['year_total'])
        data.append(temp)

    curr_data = {'month_total': [], 'year_total' : 0}
    curr_data['month_total'] = [0] * 12
    for m in range(1, 12+1):
        d_dics = WasteData.objects.filter(waste_type=wti.waste_greek, date_added__year=max_year, date_added__month=m).values('waste_kg')
        d_old = [d['waste_kg'] for d in d_dics]
        d = [float(j) for j in d_old]
        
        if d:
            curr_data['month_total'][m-1] += round(sum(d), 2)
            
    curr_data['year_total'] = round(sum(curr_data['month_total']), 2) / 1000
    year_totals.append(curr_data['year_total'])

    old_data = []
    for i in range(len(data)):
        temp = {'year': data[i]['year'], 'data': []}
        temp_dic = {"label": data[i]['year'], "backgroundColor": wti.chart_color_background, "data": data[i]['month_total']}
        temp['data'].append(temp_dic)

        if data[i]['year'] - 1 >= min_year:
            temp_old = {"label": data[i-1]['year'], "backgroundColor": wti.chart_color_border, "data": data[i-1]['month_total']}
            temp['data'].append(temp_old)
        
        if data[i]['year'] - 2 >= min_year:
            temp_old = {"label": data[i-2]['year'], "backgroundColor": wti.chart_color_border, "data": data[i-2]['month_total']}
            temp['data'].append(temp_old)

        old_data.append(temp)

    context = {'wti': wti, 'wt': wt, 'curr_year': max_year, 'last_year': max_year-1, 'months': months, 'year_totals': year_totals, 'years': years, 'data': data, 'curr_data' : curr_data, 'old_data': old_data}

    return render(request, 'data_manage/waste_info.html', context)
    

def add_data(request):
    wt = WasteType.objects.all()
    data = {'wt': wt}

    # kgs = [78770, 61910, 91570, 78990, 101070, 90180, 72880, 58260, 90490, 93500, 91160]
    # dates = ['2020-01-01', '2020-02-01', '2020-03-01', '2020-04-01', '2020-05-01', '2020-06-01', '2020-07-01', '2020-08-01', '2020-09-01', '2020-10-01', '2020-11-01']
    # wtype = 'Ανακυκλώσιμα'

    # for i in range(11):
    #     WasteData.objects.create(waste_type=wtype, waste_kg=kgs[i], date_added=dates[i])

    r = requests.get('https://wasteapi.intelligentcity.gr/wastebuckets')
    # print(r.json())

    return render(request, 'data_manage/add_data.html', data)

def submit_data(request):
    #id_data = request.POST['data-id']
    data_type = request.POST['data-type']
    kg = request.POST['data-kg']
    date = request.POST['data-date']
    
    parsed_date = date.split('-')

    if 'data-file' in request.FILES:
        file_uuid = str(uuid.uuid4())
        proof_file = request.FILES['data-file']
        new_name =  data_type + '_' + date + '_' + file_uuid + '.pdf'
        path = 'proof/' + str(parsed_date[0]) + '/' + str(parsed_date[1]) + '/' + new_name 
        file_name = default_storage.save(path, proof_file)

        FileManager.objects.create(old_file_name=proof_file.name, new_file_name=new_name, file_path=file_name, user=request.user, 
                        file_size=default_storage.size(file_name), date_referred=date)

    WasteData.objects.create(waste_type=data_type,
                             waste_kg=kg, date_added=date)

    return render(request, 'data_manage/submit_data.html', {})

def submit_bucket_add(request):
    # id_bucket = request.POST['bucket-id']
    waste_type = request.POST['bucket-type-add']
    bucket_num = request.POST['bucket-num-add']

    time = datetime.now()
    date = time.strftime('%Y-%m-%d %H:%M:%S')

    WasteBuckets.objects.filter(waste_type=waste_type).update(buckets_num=bucket_num, last_update=date)

    return render(request, 'data_manage/submit_bucket.html', {})

def submit_bucket_rem(request):
    # id_bucket = request.POST['bucket-id']
    waste_type = request.POST['bucket-type-rem']
    bucket_num = request.POST['bucket-num-rem']

    time = datetime.now()
    date = time.strftime('%Y-%m-%d %H:%M:%S')

    WasteBuckets.objects.filter(waste_type=waste_type).update(buckets_num=bucket_num, last_update=date)

    return render(request, 'data_manage/submit_bucket.html', {})

def submit_file(request):
    date = datetime.today().strftime('%Y-%m-%d')
    parsed_date = date.split('-')

    data_file = request.FILES['file-data']
    new_name =  date + '.xlsx'
    path = 'data/' + str(parsed_date[0]) + '/' + str(parsed_date[1]) + '/' + new_name 
    file_name = default_storage.save(path, data_file)

    # df = pd.read_excel(file_name)
    # print(df)


    return render(request, 'data_manage/submit_file.html', {"n": new_name})


def submit_new_type(request):
    new_type_pun = request.POST['new-type-pun']
    new_type_non_pun = request.POST['new-type-non-pun']

    WasteType.objects.create(waste_greek_no_pun=new_type_non_pun, waste_greek=new_type_pun, waste_color="secondary", chart_color_border="rgba(133, 135, 150, 1)", chart_color_background="rgba(133, 135, 150, 0.05)", waste_icon="fas fa-trash-restore fa-2x text-gray-300")

    return render(request, 'data_manage/submit_new_type.html', {})


def edit_data(request):
    # wd = WasteData.objects.all().order_by('date_added')
    wb = WasteBuckets.objects.all().order_by('last_update')
    wt = WasteType.objects.all().values()

    wd = []

    for t in wt:
        wd.append({'type': t['waste_greek'], 'id': t['idwaste_translation'], 'data': WasteData.objects.filter(waste_type=t['waste_greek']).order_by('date_added')})

    data = {"wd": wd, "wb": wb, "wt": wt}

    return render(request, 'data_manage/edit_data.html', data)


@csrf_exempt
def update_data(request):
    data_id = request.POST['data_id']

    # data_type = request.POST['data-type']
    kg = request.POST['data_kg']
    date = request.POST['data_date']

    WasteData.objects.filter(pk=data_id).update(waste_kg=kg, date_added=date)

    return JsonResponse({'msg': 'success'}, status=200)


@csrf_exempt
def update_bucket(request):
    bucket_id = request.POST['bucket_id']

    # data_type = request.POST['data-type']
    num = request.POST['bucket_num']
    date = request.POST['bucket_date']

    WasteBuckets.objects.filter(pk=bucket_id).update(
        buckets_num=num, date_added=date)

    return JsonResponse({'msg': 'success'}, status=200)


@csrf_exempt
def delete_data(request):
    data_id = request.POST['data_id']

    WasteData.objects.get(pk=data_id).delete()

    return JsonResponse({'msg': 'success'}, status=200)


@csrf_exempt
def delete_bucket(request):
    bucket_id = request.POST['bucket_id']

    WasteBuckets.objects.get(pk=bucket_id).delete()

    return JsonResponse({'msg': 'success'}, status=200)


@csrf_exempt
def edit_waste_data(request):
    d = request.POST['data_id']
    inst = WasteData.objects.get(pk=d)
    ewd = serializers.serialize("json", [inst, ])

    return JsonResponse({"instance": ewd, "wasteid": d}, status=200)


@ csrf_exempt
def edit_waste_bucket(request):
    d = request.POST['bucket_id']
    inst = WasteBuckets.objects.get(pk=d)
    ewb = serializers.serialize("json", [inst, ])

    return JsonResponse({"instance": ewb, "bucketid": d}, status=200)

@ csrf_exempt
def get_bucket_num(request):
    d = request.POST['bucket_type']
    inst = WasteBuckets.objects.filter(waste_type=d).values('buckets_num')
    
    bn = [b['buckets_num'] for b in inst]

    return JsonResponse({"bucketnum": bn[0]}, status=200)


def manage_files(request):
    wt = WasteType.objects.all()

    # file_years = []
    # for f in FileManager.objects.all():
    #     if f.date_referred.year not in file_years:
    #         file_years.append(f.date_referred.year)

    # print(file_years)

    # file_months = []
    # for f in FileManager.objects.filter(date_referred__year = 2020):
    #     str_date = f.date_referred.strftime("%Y-%m-%d")
    #     parsed_date = str_date.split('-')
    #     if parsed_date[1] not in file_months:
    #         file_months.append(parsed_date[1])

    # print(file_months)

    # for f in FileManager.objects.filter(date_referred__year = 2020):
    #     str_date = f.date_referred.strftime("%Y-%m-%d")
    #     parsed_date = str_date.split('-')
    #     if parsed_date[1] not in file_months:
    #         file_months.append(parsed_date[1])

    # data = {"wt": wt, "fy" : file_years}

    # xl = default_storage.open('data/2020/12/2020-12-18.xlsx', mode='rb')
    # df = pd.read_excel(xl, engine='openpyxl')

    # xl_titles = df.columns.tolist()

    # oikiaka_data = df[df[xl_titles[8]] == 'OIKIAKA']
    # oikiaka_data = oikiaka_data.drop(columns=[xl_titles[0], xl_titles[3], xl_titles[4], xl_titles[8]])

    # ogkodi_data = df[df[xl_titles[-2]] == 'OΓΚΩΔΗ']
    # ogkodi_data = ogkodi_data.drop(columns=[xl_titles[0], xl_titles[3], xl_titles[4], xl_titles[8]])

    # test_date = oikiaka_data.iloc[0,0] # row, col
    # parsed_date = test_date.split(' ')
    # parsed_date = parsed_date[1].split('/')
    # #check to convert from xx to 20xx
    # parsed_date = '20'+ parsed_date[2] + '-' + parsed_date[1] + '-' + parsed_date[0]
    # conv_date = datetime.strptime(parsed_date, '%Y-%m-%d')

    # print(conv_date) 
    # print(ogkodi_data)
    # print(df)

    # session = boto3.session.Session()
    # client = session.client('s3',
    #                     region_name='fra1',
    #                     endpoint_url='https://fra1.digitaloceanspaces.com',
    #                     aws_access_key_id='7OTDOIKM37XG775MSK55',
    #                     aws_secret_access_key='EBh6c13VPIcH0iAt/L/T2UhFr2H60zR0CaVUBurziws')

    # response = client.list_buckets()
    # for space in response['Buckets']:
    #     print(space['Name'])

    # response = client.list_objects(Bucket='wastemanegement')
    # for obj in response['Contents']:
    #     print(obj['Key'])

    files = FileManager.objects.all()

    
    return render(request, "data_manage/manage_files.html", {'files': files})

def open_pdf(request):
    # print(request.user)
    return FileResponse(default_storage.open('data/2020/12/2020-12-18.xls', mode='rb'), content_type='text/csv')

def waste_table(request):
    wt = WasteType.objects.all().order_by('idwaste_translation').values()
    
    min_year = datetime.now().year-4
    max_year = datetime.now().year
    years = [*range(min_year, max_year+1)]
    wd_data = []
    rec_perc = []

    for y in range(min_year, max_year + 1):
        temp = {'year' : y, 'total': 0, 'perc_contr': 100, 'perc_change': 0, 'data': []}
        for i in wt:
            d = {'type' : i['waste_greek'], 'total': 0, 'perc_contr': 0, 'perc_change': 0}
            temp['data'].append(d)
        wd_data.append(temp)

        temp = {'year' : y, 'perc': 0}
        rec_perc.append(temp)

    for i in wd_data:
        for wd in i['data']:
            monthd = [0] * 12

            for m in range(1, 12+1):
                d_dics = WasteData.objects.filter(waste_type=wd['type'], date_added__year=i['year'], date_added__month=m).values('waste_kg')
                d_old = [d['waste_kg'] for d in d_dics]
                d = [float(j) for j in d_old]
                
                if d:
                    monthd[m-1] += round(sum(d), 2)
            wd['total'] += round(sum(monthd), 2)
            i['total'] += wd['total']
        

    for i in wd_data:
        for wd in i['data']:
            if i['total']:
                wd['perc_contr'] =  round(wd['total'] * 100 / i['total'], 2)

            if i['year'] > min_year:
                ydp = 0
                for t in wd_data:
                    if t['year'] == i['year']-1:
                        for d in t['data']:
                            if d['type'] == wd['type']:
                                ydp = d['total']

                if ydp:
                    wd['perc_change'] =  round((wd['total'] - ydp) * 100 / ydp, 2)

    percs = []
    for rp in rec_perc:
        y = rp['year']
        t = 0

        for i in wd_data:
            if i['year'] == y:
                for d in i['data']:
                    if d['type'] == 'Σύμμεικτα' or d['type'] == 'Σύμμεικτα από ΚΔΑΥ' or d['type'] == 'Ογκώδη Απόβλητα προς ΧΥΤΑ':
                        t += d['total']
                
                if i['total']:
                    p = (i['total'] - t) / i['total'] * 100
                else:
                    p = 0
                rp['perc'] = (round(p, 2))
                percs.append(rp['perc'])

    data = {'wd_data' : wd_data, 'years': years, 'rec_perc': rec_perc, 'wt': wt, 'percs': percs}
    # print(data)
    return render(request, "data_manage/waste_table.html", data)

def route_data(request):
    wt = WasteType.objects.all().order_by('idwaste_translation').values()
    
    data = {'wt': wt}
    return render(request, "data_manage/route_data.html", data)


def test_excel(request):
    wt = WasteType.objects.all().order_by('idwaste_translation').values()

    data = {'wt' : wt}

    return render(request, "data_manage/test_excel.html", data)