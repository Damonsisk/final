from django.contrib import admin
from .models import WasteBuckets, WasteData, WasteType


@admin.register(WasteBuckets)
class WasteBucketsAdmin(admin.ModelAdmin):
    list_display = ("idwaste_buckets", "waste_type",
                    "buckets_num", "last_update")


@admin.register(WasteData)
class WasteDataAdmin(admin.ModelAdmin):
    list_display = ("idwaste_data", "waste_type", "waste_kg", "date_added")


@admin.register(WasteType)
class WasteTypeAdmin(admin.ModelAdmin):
    list_display = ("idwaste_translation", "waste_greek_no_pun",
                    "waste_greek", "waste_color", "waste_icon", "chart_color_border", "chart_color_background")
