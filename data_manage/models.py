from django.db import models
from datetime import date

class DataPeriod(models.Model):
    iddata_period = models.AutoField(db_column='idData_period', primary_key=True)  # Field name made lowercase.
    waste_type = models.CharField(max_length=45)
    year = models.PositiveIntegerField()
    period = models.PositiveIntegerField() # 0 - no data for year, 1 - monthly, 2 - weekly, 3 - daily

    class Meta:
        db_table = 'data_period'

class FileManager(models.Model):
    idfile_manager = models.AutoField(primary_key=True)
    old_file_name = models.CharField(max_length=45)
    user = models.CharField(max_length=45)
    date_uploaded = models.DateField(default=date.today)
    file_size = models.PositiveIntegerField()
    date_referred = models.DateField()
    new_file_name = models.CharField(unique=True, max_length=45)
    file_path = models.CharField(unique=True, max_length=45)

    class Meta:
        db_table = 'file_manager'


class WasteBuckets(models.Model):
    # Field name made lowercase.
    idwaste_buckets = models.AutoField(
        db_column='idWaste_buckets', primary_key=True)
    waste_type = models.CharField(max_length=45)
    buckets_num = models.PositiveIntegerField()
    last_update = models.DateTimeField()

    class Meta:
        db_table = 'waste_buckets'


class WasteData(models.Model):
    # Field name made lowercase.
    idwaste_data = models.AutoField(db_column='idWaste_data', primary_key=True)
    waste_type = models.CharField(max_length=45)
    waste_kg = models.DecimalField(max_digits=10, decimal_places=2)
    date_added = models.DateField()

    class Meta:
        db_table = 'waste_data'


class WasteType(models.Model):
    # Field name made lowercase.
    idwaste_translation = models.AutoField(
        db_column='idWaste_translation', primary_key=True)
    waste_greek_no_pun = models.CharField(unique=True, max_length=45)
    waste_greek = models.CharField(unique=True, max_length=45)
    waste_color = models.CharField(unique=True, max_length=45)
    waste_icon = models.CharField(unique=True, max_length=45)
    chart_color_border = models.CharField(max_length=45)
    chart_color_background = models.CharField(max_length=45)

    class Meta:
        db_table = 'waste_type'
