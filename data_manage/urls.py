from django.contrib import admin
from django.urls import path, include, re_path
from . import views

urlpatterns = [
    path('data/<int:year>', views.year_data, name='year_data'),
    path('info/<int:wtype>', views.waste_info, name='waste_info'),
    re_path(r'add_data/?', views.add_data, name='add_data'),
    re_path(r'edit_data/?', views.edit_data, name='edit_data'),
    path('submit_data/', views.submit_data, name='submit_data'),
    path('submit_bucket_add/', views.submit_bucket_add, name='submit_bucket_add'),
    path('submit_bucket_rem/', views.submit_bucket_rem, name='submit_bucket_rem'),
    path('submit_file/', views.submit_file, name='submit_file'),
    path('submit_new_type/', views.submit_new_type, name='submit_new_type'),
    path('update_data/', views.update_data, name="update_data"),
    path('update_bucket/', views.update_bucket, name="update_bucket"),
    path('delete_data/', views.delete_data, name='delete_data'),
    path('delete_bucket/', views.delete_bucket, name="delete_bucket"),
    path('get_bucket_num/', views.get_bucket_num, name="get_bucket_num"),
    re_path(r'edit_waste_data/?', views.edit_waste_data, name='edit_waste_data'),
    re_path(r'edit_waste_bucket/?', views.edit_waste_bucket,
            name='edit_waste_bucket'),
    re_path(r'manage_files/?', views.manage_files, name='manage_files'),
    path('open_pdf/', views.open_pdf, name='open_pdf'),
    path('waste_table/', views.waste_table, name='waste_table'),
    path('route_data/', views.route_data, name='route_data'),
    path('test_excel/', views.test_excel, name='test_excel'),
]
