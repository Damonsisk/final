from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path, include, re_path
from main.views import error_404

urlpatterns = [
    re_path(r'^admin/?', admin.site.urls),
    re_path(r'^login/?', auth_views.LoginView.as_view(
        template_name='users/login.html'), name="login"),
    re_path(r'^logout/?', auth_views.LogoutView.as_view(
        template_name='users/logout.html'), name="logout"),
    re_path(r'auth/?', auth_views.LoginView.as_view(
        template_name="users/login.html"), name="login_auth"),
    re_path('', include("main.urls")),
    path('', include("data_manage.urls")),
    path('', include("users.urls")),
    path('', include("predictions.urls")),
    path('', include("tests.urls")),
    #re_path(r'^.*$', error_404, name='error_404'),
]
