from django.shortcuts import render
from datetime import datetime
from data_manage.models import WasteData, WasteType, WasteBuckets
from statistics import mean

months = ['Ιανουάριος', 'Φεβρουάριος', 'Μάρτιος', 'Απρίλιος', 'Μάιος', 'Ιούνιος',
          'Ιούλιος', 'Αύγουστος', 'Σεπτέμβριος', 'Οκτώβριος', 'Νοέμβριος', 'Δεκέμβριος']

months_closed = ['Ιανουαρίου', 'Φεβρουαρίου', 'Μαρτίου', 'Απριλίου', 'Μαϊου', 'Ιουνίου',
          'Ιουλίου', 'Αυγούστου', 'Σεπτεμβρίου', 'Οκτωβρίου', 'Νοεμβρίου', 'Δεκεμβρίου']

days = ['Δευτέρα', 'Τρίτη', 'Τετάρτη', 'Πέμπτη', 'Παρασκευή', 'Σάββατο', 'Κυριακή']

def index(request):
    today = str(days[int(datetime.today().weekday())]) + ' ' + str(datetime.today().day) + ' ' + str(months_closed[datetime.today().month-1]) + ' ' + str(datetime.today().year)
    #today = datetime.today().strftime('%A %d %B %Y')
    # wt = WasteType.objects.all().order_by('waste_greek')
    wt1 = WasteType.objects.all().order_by('idwaste_translation').values()
    w_types = WasteType.objects.all().order_by('waste_greek').values('waste_greek')
    types = [d['waste_greek'] for d in w_types]

    latest_data = []
    for i in types:
        latest_data.append(WasteData.objects.filter(waste_type=i).order_by('-date_added').values('date_added').first())
    # print(latest_data)
    temp = [x for x in latest_data if x]
    ld = [x['date_added'] for x in temp]
    last_update_datetime = max(ld)
    last_update = str(days[int(last_update_datetime.weekday())]) + ' ' + str(last_update_datetime.day) + ' ' + str(months_closed[last_update_datetime.month-1]) + ' ' + str(last_update_datetime.year)

    # for i in range(0, len(wt1)):
    #     if latest_data[i]:
    #         wt1[i].update(latest_data[i])

    year = datetime.now().year
    test_max = WasteData.objects.filter(date_added__year=year).values('waste_kg')
    
    if test_max:
        max_year = year
    else:
        max_year = year-1
    min_year = 2015
    years = [*range(min_year, max_year)]

    year_data = {'year_total' : {}, 'month_total' : {}}
    type_data = []

    for i in wt1:
        data = { 'type' : i['waste_greek'], 'year_total' : {}, 'month_total' : {} }
        type_data.append(data)

    for y in range(min_year, max_year + 1):
        for t in type_data:
            t['month_total'][y] = [0] * 12
            for m in range(1, 12+1):
                d_dics = WasteData.objects.filter(waste_type=t['type'], date_added__year=y, date_added__month=m).values('waste_kg')
                d_old = [d['waste_kg'] for d in d_dics]
                d = [float(j) for j in d_old]
                
                if d:
                    t['month_total'][y][m-1] += round(sum(d), 2)
            
            t['year_total'][y] = round(sum(t['month_total'][y]), 2)
        
        year_data['month_total'][y] = [0] * 12
        for i in type_data:
            for m in range(0, 12):
                year_data['month_total'][y][m] += i['month_total'][y][m]

        year_data['year_total'][y] = round(sum(year_data['month_total'][y]), 2)
            

    wd = {'year_data' : year_data, 'type_data' : type_data}

    last_5_rec_total = []
    last_5_years = []
    for y in range(max_year-4, max_year+1):
        last_5_years.append(y)
        t = 0
        for i in wd['type_data']:
            if i['type'] == 'Σύμμεικτα' or i['type'] == 'Σύμμεικτα από ΚΔΑΥ' or i['type'] == 'Ογκώδη Απόβλητα προς ΧΥΤΑ':
               t += i['year_total'][y]

        if wd['year_data']['year_total'][y]:
            p = (wd['year_data']['year_total'][y] - t) / wd['year_data']['year_total'][y]
        else:
            p = 0
        p *= 100
        last_5_rec_total.append(round(p, 2))
    

    type_perc = []
    for t in wd['type_data']:
        data = {'type' : t['type'], 'perc': 0}
        if (wd['year_data']['year_total'][max_year]):
            data['perc'] = round((t['year_total'][max_year] * 100 / wd['year_data']['year_total'][max_year]), 2)
        type_perc.append(data)

    type_perc_pie = {'type' : [], 'perc': []}
    for t in wd['type_data']:
        type_perc_pie['type'].append(t['type'])
        if (wd['year_data']['year_total'][max_year]):
            type_perc_pie['perc'].append(round((t['year_total'][max_year] * 100 / wd['year_data']['year_total'][max_year]), 2))
        else:
            type_perc_pie['perc'].append(0)

    type_cov = []
    for t in wd['type_data']:
        data = {'type' : t['type'], 'cov': 0}
        if t['year_total'][max_year-1]:
            data['cov'] = round((t['year_total'][max_year] * 100 / t['year_total'][max_year-1]), 2)
        type_cov.append(data)

    curr_year_type = []
    for t in wd['type_data']:
        data = {'type' : t['type'], 'total': 0}
        data['total'] = t['year_total'][max_year]
        curr_year_type.append(data)

    mean_prev = round(mean(wd['year_data']['month_total'][max_year-1]), 2)

    mean_curr = round(mean(wd['year_data']['month_total'][max_year]), 2)

    # monthly_charts = []
    # for m in months[:int(datetime.today().month)]:
    #     temp = {'month': months[m], 'data': []}

    context = {
        'today': today,
        'last_update' : last_update,
        'years': years,
        'wt': wt1,
        'curr_months': months[:int(datetime.today().month)],
        'months': months,
        'month': months[int(datetime.today().month-1)][:-1],
        'this_year' : max_year,
        'last_year' : max_year-1,
        'curr_year' : wd['year_data']['year_total'][max_year] / 1000,
        'prev_year' : wd['year_data']['year_total'][max_year - 1]/1000,
        'last_five_years' : last_5_years,
        'last_five_data' : last_5_rec_total,
        'rec_perc' : last_5_rec_total[-1],
        'type_perc' : type_perc,
        'type_perc_pie' : type_perc_pie,
        'type_cov' : type_cov,
        'curr_year_type' : curr_year_type,
        'mean_prev' : round(mean_prev/1000, 2),
        'mean_curr' : round(mean_curr/1000, 2),
        'wd' : wd,
    }
        

    return render(request, 'main/index.html', context)


def base(request):
    wt = WasteType.objects.all().order_by('waste_greek')
    return render(request, 'main/base.html', {'wt': wt})


def error_404(exception):
    return render(exception, 'main/error_404.html', {})

def manual(request):
    wt = WasteType.objects.all().order_by('idwaste_translation').values()
    data = {'wt': wt}
    return render(request, 'main/manual.html', data)

def export_data(request):
    year_sel = request.POST.getlist('year-select')
    cat_sel = request.POST.getlist('cat-select')
    type_sel = request.POST.getlist('type-select')
    
    data = {}
    
    b_data = []
    d_data = []
    
    # print(year_sel)
    # print(cat_sel)
    # print(type_sel)
    
    if ('1' in cat_sel):
        for y in year_sel:
            start_date = "{}-01-01".format(int(y))
            end_date = "{}-12-31".format(int(y))

            for t in type_sel:
                d_data += WasteData.objects.filter(waste_type=t, date_added__range=[start_date, end_date]).order_by('date_added').values()

            for i in d_data:
                i['waste_kg'] = float(i['waste_kg'])
                i['date_added'] = i['date_added'].strftime("%d/%m/%Y")
                
            data[y] = d_data
            d_data = []            
    if ('2' in cat_sel):
        for t in type_sel:
            b_data += WasteBuckets.objects.filter(waste_type=t).order_by('last_update').values()
            
        for i in b_data:
            i['last_update'] = i['last_update'].strftime("%d/%m/%Y, %H:%M:%S")
    if (not cat_sel):
        data = []
        b_data = []        
    
    return render(request, 'main/pdf_template.html', {'data':data, 'b_data': b_data})

def export_excel(request):
    # year_sel = request.POST.getlist('year-select')
    # cat_sel = request.POST.getlist('cat-select')
    # type_sel = request.POST.getlist('type-select')

    return render(request, 'main/pdf_template.html')

def export_pdf(request):
    return render(request, 'main/pdf_template.html')